
document.addEventListener('DOMContentLoaded', function () {

    var randomX = Math.floor(Math.random() * 690);

    var randomY = Math.floor(Math.random() * 690);

    var dot = document.getElementById("dot");

    dot.style.left = randomX + "px";
    dot.style.top = randomY + "px";

    var pacman = document.getElementById("pacman");


    window.addEventListener('keydown', function (e) {
        move(e.keyCode);
        dotControl();
    })

    function dotControl() {
        var dotrect = dot.getBoundingClientRect();
        var pacmanrect = pacman.getBoundingClientRect();

        if (dotrect.top + (dotrect.height - 10) > pacmanrect.top
            && dotrect.left + (dotrect.width - 10) > pacmanrect.left
            && dotrect.bottom - (dotrect.height - 10) < pacmanrect.bottom
            && dotrect.right - (dotrect.width - 10) < pacmanrect.right
        ) {
            alert("Kazandın");
            window.location.reload();
        }
    }

    function move(keycode) {
        if (keycode == 37) {

            if (pacman.offsetLeft > 0) {
                pacman.style.left = (pacman.offsetLeft - 5) + "px";
            }
        }
        else if (keycode == 39) {

            if (pacman.offsetLeft + 48 < 700) {
                pacman.style.left = (pacman.offsetLeft + 5) + "px";
            }

        } else if (keycode == 38) {

            if (pacman.offsetTop > 0) {
                pacman.style.top = (pacman.offsetTop - 5) + "px";
            }

        } else if (keycode == 40) {

            if (pacman.offsetTop + 45 < 700) {
                pacman.style.top = (pacman.offsetTop + 5) + "px";
            }

        }
    }

})
